//
//  NavigationOptionsController.swift
//  MusicPlayer
//
//  Created by dev2 on 5/8/15.
//  Copyright (c) 2015 dev2. All rights reserved.
//

import UIKit

import Foundation

class  NavigationOptionsController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var navigationOptions: [String] = ["Music Player", "Record Sound"]
    
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.navigationOptions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = navigationOptions[indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == 0){
            
            self.performSegueWithIdentifier("segueMusicSelect", sender: nil)
        } else{
            
            self.performSegueWithIdentifier("segueRecordSound", sender: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
