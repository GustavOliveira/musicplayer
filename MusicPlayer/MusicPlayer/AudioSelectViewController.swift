//
//  AudioSelectViewController.swift
//  MusicPlayer
//
//  Created by dev2 on 5/8/15.
//  Copyright (c) 2015 dev2. All rights reserved.
//

import UIKit
import AVFoundation

class AudioSelectViewController: UIViewController {
    
    var audioOptions: [String] = []
    
    var listaDeAudio:[AVAudioPlayer] = []
    
    var audioToBePlayed: AudioFile!
    
    override func viewDidLoad() {
        
        //listing mp3 files on mainBundle
        let fileManager:NSFileManager = NSFileManager()
        let files = fileManager.enumeratorAtPath( NSBundle.mainBundle().bundlePath )
        
        while let file: AnyObject = files?.nextObject(){
            
            var fileName = file as! String
            var fileNameArray = split(fileName){$0 == "."}
            var index = fileNameArray.count - 1
            if( fileNameArray[index] == "mp3"){
                audioOptions.append(file as! String)
            }
        }
        
        navigationItem.title = "Música";
        
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    func setupAudioPlayerWithFile(file:NSString, type: NSString) -> AVAudioPlayer{
        
        var path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        var url = NSURL.fileURLWithPath(path!)
        var error: NSError?
        
        var audioPlayer:AVAudioPlayer?
        audioPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
        
        return audioPlayer!
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "segueMusicPlay"){
            
            
            let playSoundsVC: PlayerViewController = segue.destinationViewController as! PlayerViewController
            playSoundsVC.receivedAudio = audioToBePlayed
            
        }
    }
    
    func prepareMusicToBePlayed(file:NSString){
        
        var path = NSBundle.mainBundle().pathForResource(file as String, ofType: nil)
        var url = NSURL.fileURLWithPath(path!)
        var audio = AVAudioPlayer(contentsOfURL: url, error: nil)
        
        listaDeAudio.insert(audio, atIndex: (listaDeAudio.count))
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.audioOptions.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = audioOptions[indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        audioToBePlayed = AudioFile()
        for musica in audioOptions{
            audioToBePlayed.audioName.insert(musica, atIndex: audioToBePlayed.audioName.count)
            prepareMusicToBePlayed(musica)
        }
        
        audioToBePlayed.listaDeAudio = listaDeAudio
        audioToBePlayed.musicaASerTocada = indexPath.row

        self.performSegueWithIdentifier("segueMusicPlay", sender: nil)
    }
    
}