//
//  PlayerViewController.swift
//  MusicPlayer
//
//  Created by dev2 on 5/6/15.
//  Copyright (c) 2015 dev2. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController {

    var timerCount = ""
    var timer = NSTimer()
    var start: Bool = true
    var buttonBeep = AVAudioPlayer()
    var repeat: Bool = false
    var volume = 1
    var receivedAudio: AudioFile!
    var listaDeAudio:[AVAudioPlayer] = []
    var randomAtivado = false
    var posicaoRandomica: Int!
    var audioEngine: AVAudioEngine!
    
    @IBOutlet weak var nomeMusica: UILabel!
    @IBOutlet weak var volumeAtual: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var botaoRepetir: UIButton!
    @IBOutlet weak var botaoPlay: UIButton!
    @IBOutlet weak var musicPlaying: UISlider!
    @IBOutlet weak var randomMusic: UIButton!

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        exibirVolume()
        audioEngine = AVAudioEngine()
        timerLabel.backgroundColor = UIColor(patternImage: UIImage(named: "field_time.png")!)
        buttonBeep = self.setupAudioPlayerWithFile("ButtonTap", type:"wav")
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func startButton(sender: UIButton) {
        
        start = !start
        
        if !start {
            
            self.view.backgroundColor = UIColor.grayColor()
            botaoPlay.setImage(UIImage(named: "stop.png"), forState: UIControlState.Normal)
            inicializarMusica()
            
        } else{
            
            pausar()
            self.view.backgroundColor = UIColor.whiteColor()
            botaoPlay.setImage(UIImage(named: "play.png"), forState: UIControlState.Normal)
            musicPlaying.value = 0
            timerLabel.text = "00:00"
        }
        
    }
    
    
    @IBAction func backwardSong(sender: UIButton) {
        
        if receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].playing{
            
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].pause()
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].currentTime = 0
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
        }
    }
    
    
    @IBAction func advanceSong(sender: UIButton) {
        
        if randomAtivado{
            
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].pause()
            randomNumber()
            passarMusica(receivedAudio)
            
        } else{
            
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].currentTime += 1
        }

    }
    
    
    @IBAction func advanceSlider(sender: UISlider) {
        
        let currentInterval = NSTimeInterval(musicPlaying.value)
        receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].currentTime = currentInterval
        let tempoAtual = getTempoAtualMusica()
        musicPlaying.value = tempoAtual
    }
    
    @IBAction func diminuirVolume(sender: UIButton) {
        
        if(volume > 0){
            
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].volume -= 0.5
            volume--
            exibirVolume()
        }
    }
    
    @IBAction func aumentarVolume(sender: UIButton) {
        
        if volume < 5{
            
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].volume += 0.5
            volume++
            exibirVolume()
        }
    }
    
    
    @IBAction func repeatMusic(sender: UIButton) {
        
        repeat = !repeat
        
        if repeat{
            
            botaoRepetir.setImage(UIImage(named: "repeatAtivado.png"), forState: UIControlState.Normal)
            
        } else{
            
            botaoRepetir.setImage(UIImage(named: "repeat.png"), forState: UIControlState.Normal)
        }
    }
    
    @IBAction func passarMusica(sender: AnyObject) {
        
        if receivedAudio.musicaASerTocada < receivedAudio.listaDeAudio.count-1{
            
            pausar()
            receivedAudio.musicaASerTocada += 1
            ajustarTempoMusica()
            randomNumber()
            ajustarAudioASerTocad()
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
            atualizarNomeMusica()
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("atualizaProgressBar:"), userInfo: nil, repeats: true)
            
        } else{
            
            pausar()
            receivedAudio.musicaASerTocada = 0
            ajustarTempoMusica()
            randomNumber()
            ajustarAudioASerTocad()
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
            atualizarNomeMusica()
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("atualizaProgressBar:"), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func retornarMusica(sender: AnyObject) {
        
        if receivedAudio.musicaASerTocada > 0{
            
            pausar()
            receivedAudio.musicaASerTocada -= 1
            ajustarTempoMusica()
            randomNumber()
            ajustarAudioASerTocad()
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
            atualizarNomeMusica()
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("atualizaProgressBar:"), userInfo: nil, repeats: true)
            
        }else{
            
            pausar()
            receivedAudio.musicaASerTocada = receivedAudio.listaDeAudio.count-1
            ajustarTempoMusica()
            receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
            atualizarNomeMusica()
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("atualizaProgressBar:"), userInfo: nil, repeats: true)
        }
        
    }
    
    @IBAction func randomMusic(sender: AnyObject) {
        
        randomAtivado = !randomAtivado
        
        if randomAtivado{
            
            randomMusic.setImage(UIImage(named: "randomAtivado"), forState: UIControlState.Normal)
            
        } else{
            
            randomMusic.setImage(UIImage(named: "random"), forState: UIControlState.Normal)
            
        }
        
    }
    
    func inicializarMusica(){
        
        ajustarAudioASerTocad()
        receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].volume = 0.5
        receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
        atualizarNomeMusica()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("atualizaProgressBar:"), userInfo: nil, repeats: true)
        
    }
    
    
    func atualizaProgressBar(sender: AnyObject) {
        
        if receivedAudio.listaDeAudio.count != 0{
            let tempoDaMusica = stringFromTimeInterval(receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].currentTime)
            timerLabel.text = "\(tempoDaMusica)"
            musicPlaying.value = getTempoAtualMusica()
            musicPlaying.maximumValue = getTempoTotalMusica()
            
            if (!(receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].playing) && repeat){
                
                receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].play()
                
            } else if(!receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].playing){
                
                passarMusica(receivedAudio)
            }

        }
        
    }
    
    func atualizarNomeMusica(){
        
        nomeMusica.text = receivedAudio.audioName[receivedAudio.musicaASerTocada]
    }
    
    func pausar(){
        
        receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].pause()
        timer.invalidate()
    }
    
    func exibirVolume(){
        
        volumeAtual.text = "Volume: \(volume)"
        volumeAtual.highlighted = true
    }
    
    func getTempoAtualMusica() -> Float{
        
        let cur = receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].currentTime
        let tempoAtual = Float(cur)
        
        return tempoAtual
    }
    
    func getTempoTotalMusica() -> Float{
        
        let dur = receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].duration
        let tempoTotal = Float(dur)
        
        return tempoTotal
    }
    
    func ajustarAudioASerTocad(){
        
        if randomAtivado{
            
            receivedAudio.musicaASerTocada = posicaoRandomica
        }
    }
    
    func ajustarTempoMusica(){
        
        receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].stop()
        receivedAudio.listaDeAudio[receivedAudio.musicaASerTocada].currentTime = 0
    }
    
    func randomNumber(){
        
        if randomAtivado{
            
            posicaoRandomica = Int(arc4random_uniform(UInt32(receivedAudio.listaDeAudio.count)))
        }
    }
    
    func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        
        var ti = NSInteger(interval)
        var ms = Int((interval % 1) * 1000)
        var seconds = ti % 60
        var minutes = (ti / 60) % 60
        var hours = (ti / 3600)
        
        return NSString(format: "%0.2d:%0.2d",minutes,seconds)
    }
    
    func setupAudioPlayerWithFile(file:NSString, type: NSString) -> AVAudioPlayer{
        
        var path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        var url = NSURL.fileURLWithPath(path!)
        var error: NSError?
        
        var audioPlayer:AVAudioPlayer?
        audioPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
        
        return audioPlayer!
    }
    
    override func viewWillAppear(animated: Bool) {
      navigationItem.title = nil;
    }
    
    override func viewWillDisappear(animated: Bool) {
        ajustarTempoMusica()
        timer.invalidate()
    }
    
}

