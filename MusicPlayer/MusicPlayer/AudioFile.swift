//
//  RecordAudio.swift
//  RecordSound
//
//  Created by dev2 on 5/8/15.
//  Copyright (c) 2015 dev2. All rights reserved.
//

import Foundation
import AVFoundation

class AudioFile: NSObject{
    
    var filePathUrl: NSURL!
    var title: String!
    
    var audioName: [String] = []
    var listaDeAudio:[AVAudioPlayer] = []
    var musicaASerTocada: Int = 0
}