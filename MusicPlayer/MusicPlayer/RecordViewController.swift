//
//  RecordViewController.swift
//  MusicPlayer
//
//  Created by dev2 on 5/8/15.
//  Copyright (c) 2015 dev2. All rights reserved.
//

import UIKit
import AVFoundation


class RecordViewController: UIViewController, AVAudioRecorderDelegate {
    
    var timer = NSTimer()
    var audioRecorder: AVAudioRecorder!
    var recordAudio: AudioFile!
    
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var buttonBeep: UIButton!
    @IBOutlet weak var buttonStop: UIButton!
    @IBOutlet weak var recording: UILabel!
    
    
    
    @IBAction func recordSound(sender: UIButton) {
        
        self.view.backgroundColor = UIColor.grayColor()
        recording.hidden = false
        timerLabel.hidden = false
        let dirPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "ddMMyyyy-HHmmss"
        
        let recordingName = formatter.stringFromDate(currentDateTime)+".wav"
        let pathArray: [AnyObject] = [dirPath, recordingName]
        let filePath = NSURL.fileURLWithPathComponents(pathArray)
        
        println(filePath)
        
        var session = AVAudioSession.sharedInstance()
        session.setCategory(AVAudioSessionCategoryPlayAndRecord, error: nil)
        
        audioRecorder = AVAudioRecorder(URL: filePath, settings: nil, error: nil)
        audioRecorder.delegate = self
        audioRecorder.meteringEnabled = true;
        audioRecorder.prepareToRecord()
        audioRecorder.record();
        
        audioRecorder.recording
        
        buttonBeep.hidden = true
        buttonStop.hidden = false
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("atualizarTempoGravacao:"), userInfo: nil, repeats: true)
    }
    
    func atualizarTempoGravacao(sender: AnyObject){
        let tempoDaMusica = stringFromTimeInterval(audioRecorder.currentTime)
        timerLabel.text = "\(tempoDaMusica)"
    }
    
    func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        
        var ti = NSInteger(interval)
        var ms = Int((interval % 1) * 1000)
        var seconds = ti % 60
        var minutes = (ti / 60) % 60
        var hours = (ti / 3600)
        
        return NSString(format: "%0.2d:%0.2d",minutes,seconds)
    }
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
        
        if (flag){
            let currentDateTime = NSDate()
            
            recordAudio = AudioFile();
            recordAudio.filePathUrl = recorder.url
            recordAudio.title = recorder.url.lastPathComponent;
            
            recordAudio.audioName.insert("Gravação_\(currentDateTime)", atIndex: 0)
            
            var audio = AVAudioPlayer(contentsOfURL: recordAudio.filePathUrl, error: nil)
            recordAudio.listaDeAudio.insert(audio, atIndex: recordAudio.listaDeAudio.count)
            
            self.performSegueWithIdentifier("seguePlayRecord", sender: recordAudio)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "seguePlayRecord"){
            let playSoundsVC: PlayerViewController = segue.destinationViewController as! PlayerViewController
            let data = sender as! AudioFile
            playSoundsVC.receivedAudio = data
            
        }
    }
    
    
    @IBAction func stopAudio(sender: UIButton) {
        
        recording.hidden = true
        timerLabel.hidden = true
        self.view.backgroundColor = UIColor.grayColor()
        audioRecorder.stop()
        var audioSession = AVAudioSession.sharedInstance()
        audioSession.setActive(false, error: nil)

    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        timerLabel.backgroundColor = UIColor(patternImage: UIImage(named: "field_time.png")!)
        timerLabel.hidden = true
        buttonStop.hidden = true
        recording.hidden = true
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}